package com.tgc.demo.featureflag.controller;

import com.tgc.demo.featureflag.config.Features;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.togglz.core.manager.FeatureManager;

@RestController
public class FeatureFlagController {

    private FeatureManager featureManager;

    public FeatureFlagController(FeatureManager featureManager) {
        this.featureManager = featureManager;
    }

    @GetMapping("hello")
    public String helloWorld(@RequestParam String name) {
        StringBuilder builder = new StringBuilder();
        builder.append("Hello ");
        builder.append(name);
        builder.append(" !!!");

        builder.append("\nAllowed features - ");
        if(featureManager.isActive(Features.FEATURE_ONE)) {
            builder.append(Features.FEATURE_ONE.name());
            builder.append("\t");
        }

        if(featureManager.isActive(Features.FEATURE_TWO)) {
            builder.append(Features.FEATURE_TWO.name());
        }

        return builder.toString();
    }
}
